import { InputStream } from "./InputStream";
import { Tokenizer } from "./tokenizer/Tokenizer";
import { Parser } from "./Parser";

import * as fs from "fs";

const input = `
function yes(arg: int): bool {
  // always returns true
  hello_world();
  a = 1 + 3;
  b = c && d || e;
  return;
}

function maybe(arg: int64): bool {
  // always returns true
  yes();
  a = 1 + 3;
  b = c && d || e;
  return a + b * c - d;
}

let FOO: string = bar;
`;

try {
  console.log("// parsing //");
  const p = new Parser(new Tokenizer(new InputStream(input)));
  const ast = p.parse();
  console.log("ast", ast);
  console.log("body",(ast.nodes[0] as any)["body"])

  console.log(process.cwd());

  fs.writeFileSync("./ast.json", JSON.stringify(ast));

} catch (error) {
  if (error instanceof Error) {
    console.error(error.stack || error.message)
  } else {
    console.error(error);
  }
}

