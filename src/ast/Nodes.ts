export namespace AST {
  export interface NumberLiteral {
    type: "number";
    value: number;
  }

  export interface StringLiteral {
    type: "string";
    value: string;
  }

  export interface BoolLiteral {
    type: "boolean";
    value: boolean;
  }

  export interface Type {
    type: "type",
    name: string;
  }

  export interface Identifier {
    type: "identifier";
    name: string;
  }

  export interface FunctionParameter {
    type: "function-parameter",
    name: string,
    parameterType: Type
  }

  export interface FunctionDeclaration {
    type: "function-declaration";
    parameters: FunctionParameter[];
    name?: string;
    body: Sequence;
    returnType?: Type;
  }

  export interface FunctionReturn {
    type: "function-return";
    right?: AST.Node;
  }

  export interface Call {
    type: "call";
    function: Node;
    arguments: Node[];
  }

  export interface Conditional {
    type: "conditional";
    condition: Node;
    trueBody: Node;
    falseBody?: Node;
  }

  export interface Assignment {
    type: "assign";
    left: Node;
    right: Node;
  }

  export interface BinaryOp {
    type: "binary";
    operator: string;
    left: Node;
    right: Node;
  }

  export interface Sequence {
    type: "sequence";
    nodes: Node[];
  }

  export interface VariableDeclaration {
    type: "variable-declaration";
    identifier: Identifier;
    variableType?: Type;
    initializer?: AST.Node;
  }

  export type Node =
    NumberLiteral |
    StringLiteral |
    BoolLiteral |
    Identifier |
    FunctionDeclaration |
    FunctionParameter |
    FunctionReturn |
    Call |
    Conditional |
    Assignment |
    BinaryOp |
    Type |
    VariableDeclaration |
    Sequence;
}
