import { Stringable } from "./helpers";

export class InputStream {
  constructor(private input: string) {}

  public next(n = 1): string {
    let str = "";

    for (let i = 0; i < n; i++) {
      str += this.nextOne();
    }

    return str;
  }

  public peek(n = 1): string {
    return this.input.substr(this.pos, n);
  }

  public eof(): boolean {
    return this.peek() == "";
  }

  public croak(...args: Stringable[]): never {
    const msg = args.map(a => a.toString()).join(" ");
    throw new Error(msg + " (" + this.line + ":" + this.col + ")");
  }

  // internals /////////////

  private nextOne(): string {
    let ch = this.input.charAt(this.pos++);
    if (ch == "\n") {
      this.line++;
      this.col = 0; 
    } else {
      this.col++;
    }
    return ch;
  }

  private pos = 0;
  private line = 0;
  private col = 0;
}