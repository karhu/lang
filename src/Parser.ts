import { Tokenizer } from "./tokenizer/Tokenizer";
import { AST } from "./ast/nodes";
import { Punctuation } from "./tokenizer/Punctuations";
import { OperatorToken, PunctuationToken, KeywordToken, Token } from "./tokenizer/Tokens";
import { Operator, operatorPrecedence } from "./tokenizer/Operators";
import { Keyword } from "./tokenizer/Keywords";

export class Parser {
  constructor(private input: Tokenizer) {}

  parse(): AST.Sequence {
    return this.parseSequence();
  }

  // internals //////////////////////

  private parseSequence(): AST.Sequence {
    const input = this.input;
    const nodes = new Array<AST.Node>();

    while (!input.eof()) {
      nodes.push(this.parseExpression());
      if (this.peekPunctuation(";")) {
        this.input.next();
      }
    }

    return {
      type: "sequence",
      nodes
    };
  }

  private parseAtom(): AST.Node {
    return this.maybeCall(() => {
      if (this.peekPunctuation("(")) {
        this.input.next();
        const exp = this.parseExpression();
        this.skipPunctuation(")");
        return exp;
      }
      if (this.peekPunctuation("{")) {
        return this.parseSequence();
      }
      if (this.peekKeyword("if")) {
        return this.parseIf();
      }
      if (this.peekKeyword("true") || this.peekKeyword("false")) {
        return this.parseBoolean();
      }
      if (this.peekKeyword("function")) {
        this.input.next();
        return this.parseFunction();
      }
      if (this.peekKeyword("return")) {
        return this.parseReturn();
      }
      if (this.peekKeyword("let")) {
        return this.parseVariableDeclaration();
      }
      const tok = this.input.next();
      if (tok.type === "ident") {
        return {
          type: "identifier",
          name: tok.value
        };
      }
      if (tok.type === "num") {
        return {
          type: "number",
          value: tok.value
        }
      }
      return this.unexpected(tok);
    });
  }

  private parseExpression(): AST.Node {
    return this.maybeCall(() => {
      return this.maybeBinary(this.parseAtom(), 0);
    });
  }

  private unexpected(token?: Token): never {
    token = token ||  this.input.peek();
    return this.input.croak("Unexpected token:", JSON.stringify(token));
  }

  // punctuation ////////////////////////////////////////////////

  private skipPunctuation(value: Punctuation) {
    this.assertPunctuation(value);
    this.input.next();
  }

  private peekPunctuation(expected: Punctuation): PunctuationToken | null {
    const input = this.input;
    const token = input.peek();

    if (token.type === "punc" && (!expected || token.value === expected)) {
      return token;
    } else {
      return null;
    }
  }

  private assertPunctuation(expected: Punctuation) {
    if (!this.peekPunctuation(expected)) {
      const token = this.input.peek();
      this.input.croak(`Expected punctuation token ("${expected}"), but got ${JSON.stringify(token)}.`);
    }
  }

  // function definition /////////////////////////////////////////

  private parseFunction(): AST.FunctionDeclaration {
    const nameTok = this.input.next();

    if (nameTok.type !== "ident") {
      return this.unexpected(nameTok);
    } else {
      const parameters = this.parseDelimited<AST.FunctionParameter>("(", ")", ",", this.parseFunctionParameter.bind(this));

      let returnType: AST.Type | undefined;

      if (this.peekPunctuation(":")) {
        this.input.next();
        returnType = this.parseType()
      }

      const body = this.parseFunctionBody();

      return {
        type: "function-declaration",
        name: nameTok.value,
        returnType,
        parameters,
        body,
      }
    }
  }

  private parseFunctionBody(): AST.Sequence {
    this.skipPunctuation("{");

    const nodes = new Array<AST.Node>();

    while (!this.peekPunctuation("}")) {
      nodes.push(this.parseExpression());
      this.skipPunctuation(";");
    }

    this.skipPunctuation("}");

    return {
      type: "sequence",
      nodes
    };
  }

  private parseIdentifier(): AST.Identifier {
    const name = this.input.next();
    if (name.type != "ident") {
      return this.input.croak("Expecting variable name");
    }
    return {
      type: "identifier",
      name: name.value
    };
  }

  private parseVariableDeclaration(): AST.VariableDeclaration {
    this.skipKeyword("let");
    const identifier = this.parseIdentifier();

    let variableType: AST.Type | undefined;
    if (this.peekPunctuation(":")) {
      this.input.next();
      variableType = this.parseType();
    }

    let initializer: AST.Node | undefined;
    if (this.peekOperator("=")) {
      this.input.next();
      initializer = this.parseExpression();
    }

    return {
      type: "variable-declaration",
      identifier,
      variableType,
      initializer,
    }
  }

  private parseType(): AST.Type {
    const name = this.input.next();
    if (name.type != "ident") {
      return this.input.croak("Expecting type name");
    }
    return {
      type: "type",
      name: name.value
    };
  }

  private parseFunctionParameter(): AST.FunctionParameter {
    const name = this.input.next();
    if (name.type != "ident") {
      return this.input.croak("Expecting function parameter name");
    }
    this.skipPunctuation(":");
    const parameterType = this.parseType();

    return {
      type: "function-parameter",
      name: name.value,
      parameterType
    };
  }

  private parseReturn(): AST.FunctionReturn {
    this.skipKeyword("return");
    if (this.peekPunctuation(";")) {
      return {
        type: "function-return"
      }
    } else {
      return {
        type: "function-return",
        right: this.parseExpression()
      }
    }
  }

  private parseIf(): AST.Conditional {
    this.skipKeyword("if");
    const condition = this.parseExpression();
    const trueBody = this.parseExpression();

    let falseBody: AST.Node | undefined;
    if (this.peekKeyword("else")) {
        this.input.next();
        falseBody = this.parseExpression();
    }

    return {
      type: "conditional",
      condition,
      trueBody,
      falseBody
    };
  }

  private parseBoolean(): AST.BoolLiteral {
    const tok = this.input.next();
    if (tok.type !== "keyword" ) {
      return this.unexpected(tok);
    } else if (tok.value !== "true" && tok.value !== "false") {
      return this.unexpected(tok);
    } else {
      return {
        type: "boolean",
        value: tok.value === "true"
      };
    }

  }

  // function call ///////////////////////////////////////////////

  /**
   * Parses an expression, then checks if that expression is called.
   * Returns a call node or the plain expression
   */
  private maybeCall(parse: () => AST.Node): AST.Node {
    const expr = parse();
    return this.peekPunctuation("(") ? this.parseCall(expr) : expr;
  }

  private parseCall(func: AST.Node): AST.Call {
    return {
      type: "call",
      function: func,
      arguments: this.parseDelimited("(", ")", ",", this.parseExpression)
    };
  }

  // binary /////////////////////////////////////////////////////////////////

  private maybeBinary(left: AST.Node, myPrecedence: number): AST.Node {
    const token = this.peekOperator();
    if (token) {
      var hisPrecedence = operatorPrecedence.get(token.value) || 0;
      if (hisPrecedence > myPrecedence) {
        this.input.next();
        if (token.value === "=") {
          return this.maybeBinary({
            type: "assign",
            left,
            right: this.maybeBinary(this.parseAtom(), hisPrecedence)
          }, myPrecedence);
        } else {
          return this.maybeBinary({
            type: "binary",
            operator: token.value,
            left,
            right: this.maybeBinary(this.parseAtom(), hisPrecedence)
          }, myPrecedence);
        }
      }
    }
    return left;
  }

  private peekOperator(expected?: Operator): OperatorToken | null {
    const input = this.input;
    const token = input.peek();

    if (token.type === "op" && (!expected || token.value === expected)) {
      return token;
    } else {
      return null;
    }
  }

  // keywords ////////////////////////////////////////////////////////////////

  private peekKeyword(expected?: Keyword): KeywordToken | null {
    const input = this.input;
    const token = input.peek();

    if (token.type === "keyword" && (!expected || token.value === expected)) {
      return token;
    } else {
      return null;
    }
  }

  private skipKeyword(value: Keyword) {
    this.assertKeyword(value);
    this.input.next();
  }

  private assertKeyword(expected: Keyword) {
    if (!this.peekKeyword(expected)) {
      const token = this.input.peek();
      this.input.croak(`Expected keyword token ("${expected}"), but got ${token}.`);
    }
  }

  // etc

  private parseDelimited<T extends AST.Node>(start: Punctuation, stop: Punctuation, separator: Punctuation, parse: () => T): T[] {
    const input = this.input;
    let first = true;
    const entries = new Array<T>();

    this.skipPunctuation(start);

    while (!input.eof()) {
      // check for end of the delimited sequence
      if (this.peekPunctuation(stop)) {
        break;
      }
      // require presence of a separator after first parsed entry
      if (!first) {
        this.skipPunctuation(separator);
      }
      // allow for trailing separator
      if (this.peekPunctuation(stop)) {
        break;
      }
      // parse the entry
      entries.push(parse());
      first = false;
    }

    this.skipPunctuation(stop);

    return entries;
  }
}