import { tuple } from "../helpers";

export const allKeywords = tuple("if", "then", "else", "function", "true", "false", "return", "const", "let");
export type Keyword = typeof allKeywords[number];

export function isKeyword(x: string): x is Keyword {
  const keywords = allKeywords as string[];
  return keywords.includes(x);
}
