import { Keyword } from "./Keywords";
import { Punctuation } from "./Punctuations";
import { Operator } from "./Operators";

export interface PunctuationToken {
  readonly type: "punc";
  readonly value: Punctuation;
}

export interface NumberToken {
  readonly type: "num";
  readonly value: number;
}

export interface StringToken {
  readonly type: "str";
  readonly value: string;
}

export interface KeywordToken {
  readonly type: "keyword";
  readonly value: Keyword;
}

export interface IdentifierToken {
  readonly type: "ident";
  readonly value: string;
}

export interface OperatorToken {
  readonly type: "op";
  readonly value: Operator;
}

export interface EndToken {
  readonly type: "eof";
}

export type Token = PunctuationToken | NumberToken | StringToken | KeywordToken | IdentifierToken | OperatorToken | EndToken;

// export function isPunctuation(token: Token): token is PunctuationToken {
//   return token.type === "punc";
// }