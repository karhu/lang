import { InputStream } from "../InputStream";
import { isKeyword } from "./Keywords";
import { NumberToken, KeywordToken, IdentifierToken, StringToken, Token } from "./Tokens";
import { isPunctuation } from "./Punctuations";
import { Stringable } from "../helpers";
import { isOperator } from "./Operators";

type char = string;

export class Tokenizer {
  constructor(private input: InputStream) {}

  peek(): Token {
    if (!this.current) {
      this.current = this.readNext();
    }
    return this.current;
  }

  next(): Token {
    const token = this.current;
    this.current = null;
    return token || this.readNext();
  }

  eof(): boolean {
    return this.peek().type === "eof";
  }

  croak(...args: Stringable[]): never {
    return this.input.croak(...args);
  }

  // internals /////////////////////

  private readNext(): Token {
    const input = this.input;

    this.readWhile(isWhitespace);

    if (input.eof()) {
      return {
        type: "eof"
      };
    }

    let ch = input.peek();

    if (input.peek(2) == "//") {
      this.skipComment();
      return this.readNext();
    }

    if (ch == '"') {
      return this.readString();
    }

    if (isDigit(ch)) {
      return this.readNumber();
    }

    if (isIdStart(ch)) {
      return this.readIdentifier();
    }

    if (isPunctuation(ch)) {
      input.next();
      return {
        type  : "punc",
        value : ch
      };
    }

    if (isOpChar(ch)) {
      const op = this.readWhile(isOpChar);
      if (isOperator(op)) {
        return {
          type  : "op",
          value : op
        };
      } else {
        input.croak("Can't handle operator sequence:", op);
      }
    }

    return input.croak("Can't handle character: " + ch);
  }

  private readWhile(predicate: (ch: char) => boolean) {
    let str = "";
    while (!this.input.eof() && predicate(this.input.peek())) {
      str += this.input.next();
    }
    return str;
  }

  private readNumber(): NumberToken {
    let hasDot = false;
    let number = this.readWhile(ch => {
      if (ch == ".") {
        if (hasDot) {
          return false;
        }
        hasDot = true;
        return true;
      }
      return isDigit(ch);
    });
    return { type: "num", value: parseFloat(number) };
  }

  private readIdentifier(): KeywordToken | IdentifierToken {
    let id = this.readWhile(isId);
    if (isKeyword(id)) {
      return {
        type: "keyword",
        value: id
      };
    }
    else {
      return {
        type: "ident",
        value: id
      };
    }
  }

  private readEscaped(end: char): string {
    let escaped = false;
    let str = "";
    this.input.next(); // TODO why?

    while (!this.input.eof()) {
      let ch = this.input.next();
      if (escaped) {
          str += ch;
          escaped = false;
      } else if (ch == "\\") {
          escaped = true;
      } else if (ch == end) {
          break;
      } else {
          str += ch;
      }
    }
    return str;
  }

  private readString(): StringToken {
    return {
      type: "str",
      value: this.readEscaped('"')
    };
  }

  private skipComment(): void {
    this.readWhile(ch => { return !isNewLine(ch); });
    this.input.next(); // skip the newLine character
  }

  private current: Token | null = null;
}

function isDigit(ch: char): boolean {
  return /[0-9]/i.test(ch);
}

function isIdStart(ch: char): boolean {
  return /[a-z]/i.test(ch);
}

function isId(ch: char): boolean {
  return isIdStart(ch) || "?!-_<>=0123456789".indexOf(ch) >= 0;
}

function isOpChar(ch: char): boolean {
  return "+-*/%=&|<>!".indexOf(ch) >= 0;
}

function isWhitespace(ch: char): boolean {
  return " \t\n".indexOf(ch) >= 0;
}

function isNewLine(ch: char): boolean {
  return ch === "\n";
}