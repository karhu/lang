import { tuple } from "../helpers";

export const allPunctuations = tuple(",", ":", ";", "(", ")", "{", "}", "[", "]");
export type Punctuation = typeof allPunctuations[number];

export function isPunctuation(x: string): x is Punctuation {
  const keywords = allPunctuations as string[];
  return keywords.includes(x);
}
