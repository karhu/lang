import { tuple } from "../helpers";

export const all = tuple("=", "||", "&&", "<", ">", "<=", ">=", "==", "!=", "+", "!=", "+", "-", "*", "/", "%");
export type Operator = typeof all[number];

export function isOperator(x: string): x is Operator {
  const keywords = all as string[];
  return keywords.includes(x);
}

export const operatorPrecedence = new Map<Operator, number>([
  ["=", 1],
  ["||", 2],
  ["&&", 2],
  ["<", 7], [">", 7], ["<=", 7], [">=", 7], ["==", 7], ["!=", 7],
  ["+", 10], ["-", 10],
  ["*", 20], ["/", 20], ["%", 20],
]);