
type Literal = string | number | boolean | undefined | null | void | {};
export const tuple = <T extends Literal[]>(...args: T) => args;

export interface Stringable {
  toString(): string;
}